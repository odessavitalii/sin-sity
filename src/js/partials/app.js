$(window).on('load', function() {
	setTimeout(() => {
		$('body').removeClass('is-preload');
	}, 1000);
});

$('document').ready(function() {
	init();
});

$(window).resize(function () {
	init();
});

function init() {
	const $trigger = $('#hamburger');
	const $body = $('body');
	let isClosed = true;
	const $window = $(window);
	const $scrollSection = $('#scroll-section');
	const $nav = $('#nav-desktop');
	const $navMob = $('#nav-mobile');
	const $navLink = $nav.find('.nav__link');
	const $navLinkMob = $navMob.find('.nav__link');

	if ($window.width() < 485) {
		$trigger.on('click', function() {
			burgerTime();
		});
	}

	function burgerTime() {
		if (isClosed === true) {
			$trigger.removeClass('is-open');
			$trigger.addClass('is-closed');
			$body.removeClass('is-menu-open');
			isClosed = false;
		} else {
			$trigger.removeClass('is-closed');
			$trigger.addClass('is-open');
			$body.addClass('is-menu-open');
			isClosed = true;
		}
	}

	/* wow init */
	const wowOutside = new WOW();
	const wowInside = new WOW({
			boxClass       : 'wowInside',
			scrollContainer: '#scroll-section',
			mobile         : true,
			live           : true,
			offset         : 50
		}
	);

	setTimeout(() => {
		wowOutside.init();
		wowInside.init();
	}, 2500);

	if ($window.width() <= 991) {
		$('.wowInside').each((index, item) => {
			$(item).removeClass('wowInside').addClass('wow wow-cache');
		})
	} else {
		$('.wow-cache').each((index, item) => {
			$(item).removeClass('wow wow-cache').addClass('wowInside');
		})
	}
	/* end wow init */

	/* anchor */
	if ($window.width() > 992) {
		$navLink.on('click', function (e) {
			e.preventDefault();

			$navLink.removeClass('js-active');
			$(this).addClass('js-active');

			$scrollSection.stop().animate({
				scrollTop: $($(this).attr('href')).offset().top + $scrollSection.scrollTop() - $scrollSection.offset().top
			}, 1000);
		});
	} else if ($window.width() <= 991 && $window.width() >= 486) {
		$navLink.on('click', function (e) {
			e.preventDefault();

			$navLink.removeClass('js-active');
			$(this).addClass('js-active');

			$('html, body').stop().animate({
				scrollTop: $($(this).attr('href')).offset().top
			}, 1000);
		});
	} else {
		$navLinkMob.on('click', function (e) {
			e.preventDefault();

			$navLink.removeClass('js-active');
			$(this).addClass('js-active');

			$('html, body').stop().animate({
				scrollTop: $($(this).attr('href')).offset().top
			}, 1000);

			burgerTime();
		});
	}

	/* end anchor */

	$scrollSection.on('scroll', function () {
		detectSecInViewport();
	});

	function detectSecInViewport() {
		$('.sec').each(function (index, item) {
			let viewportTop = + $scrollSection.scrollTop() - $scrollSection.offset().top;
			let viewportBottom = viewportTop + $scrollSection.outerHeight();
			let elementTop = +($(item).offset().top.toFixed(0)) + $scrollSection.scrollTop();
			let elementBottom = elementTop + +($(item).outerHeight().toFixed(0));

			if (elementTop >= viewportTop && elementBottom >= viewportTop && elementTop <= viewportBottom && elementBottom <= viewportBottom) {
				let id = $(item).attr('id');

				// setLocation(id);

				$navLink.removeClass('js-active');
				$navLink.each(function (index, item) {
					let navHref = $(item).attr('href').substr(1).split("#")[0];

					if (navHref === id) {
						$(item).addClass('js-active')
					}
				});
			}
		})
	}

	// function setLocation(curLoc){
	//     try {
	//         history.pushState(null, null, '#' + curLoc);
	//         return;
	//     } catch(e) {}
	//     location.hash = '#' + curLoc;
	// }
}